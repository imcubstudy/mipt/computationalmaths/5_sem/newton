#include <iostream>
#include <fstream>
#include <string>
#include <algorithm>
#include <exception>
#include <utility>
#include <random>
#include <chrono>
#include <cmath>
#include <vector>
struct sqmatrix_t;

struct vec_t
{
    int     dim;
    double  *data;

    vec_t();
    vec_t(const int dim);

    vec_t(const vec_t& that);
    vec_t(vec_t&& that);
    
    ~vec_t();

    vec_t& operator=(const vec_t& that);
    vec_t& operator=(vec_t&& that);

    void setdim(const int dim);

    double&         operator[](const int idx);
    const double&   operator[](const int idx) const;

    vec_t operator*(const float b) const;
    vec_t operator/(const float b) const;

    vec_t operator+(const vec_t& that) const;
    vec_t operator-(const vec_t& that) const;

    vec_t operator-() const;
};

std::ostream& operator<<(std::ostream& os, const vec_t& v);
std::istream& operator>>(std::istream& is,       vec_t& v);

enum norm_t
{
    EUCLID = 1
};

double norm(const vec_t& v, const norm_t norm = norm_t::EUCLID);

vec_t::vec_t() 
{ 
    this->dim = 0; 
    this->data = nullptr; 
}

vec_t::vec_t(const int dim)
{
    this->dim = dim;
    this->data = new double[this->dim]{};
}

vec_t::vec_t(const vec_t& that)
{
    new(this) vec_t(that.dim);
    for(int i = 0; i < this->dim; ++i) {
        this->data[i] = that.data[i];
    }
}

vec_t::vec_t(vec_t&& that)
{
    new(this) vec_t;
    std::swap(this->data, that.data);
    std::swap(this->dim,  that.dim);   
}

vec_t::~vec_t()
{
    if(this->data != nullptr) {
        delete[] this->data;
    }
}

vec_t& vec_t::operator=(const vec_t& that)
{
    if(this->dim != that.dim) {
        throw std::runtime_error("Non-compatible assignment");
    }

    for(int i = 0; i < this->dim; ++i) {
        this->data[i] = that.data[i];
    }
    return *this;
}

vec_t& vec_t::operator=(vec_t&& that)
{
    if(this->dim != that.dim) {
        throw std::runtime_error("Non-compatible assignment");
    }
    std::swap(this->data, that.data);
    std::swap(this->dim,  that.dim);
    return *this;
}

void vec_t::setdim(const int dim)
{
    if((this->dim != dim) && (dim > 0)) {
        this->~vec_t();
        new(this) vec_t(dim);
    }
}

double& vec_t::operator[](const int idx)
{
    return this->data[idx];
}

const double& vec_t::operator[](const int idx) const
{
    return this->data[idx];
}

vec_t vec_t::operator*(const float b) const
{
    vec_t out(this->dim);
    for(int i = 0; i < out.dim; ++i) {
        out.data[i] = this->data[i] * b;
    }
    return out;
}

vec_t vec_t::operator/(const float b) const
{
    return (*this) * (1/b);
}

vec_t vec_t::operator+(const vec_t& that) const
{
    if(this->dim != that.dim) {
        throw std::runtime_error("Non-compatible binary operator");
    }

    vec_t out(this->dim);
    for(int i = 0; i < this->dim; ++i) {
        out.data[i] = this->data[i] + that.data[i];
    }
    return out;
}

vec_t vec_t::operator-(const vec_t& that) const
{
    return (*this) + (that * -1.f);
}

vec_t vec_t::operator-() const
{
    return (*this) * -1.f;
}

std::ostream& operator<<(std::ostream& os, const vec_t& v)
{
    for(int i = 0; i < v.dim; ++i) {
        os << v[i] << " ";
    }
    return os;
}

std::istream& operator>>(std::istream& is, vec_t& v)
{
    int dim = 0;
    is >> dim;
    v.setdim(dim);

    for(int i = 0; i < dim; ++i) {
        is >> v.data[i];
    }
    return is;
}

double norm(const vec_t& v, const norm_t norm)
{
    auto euclid_norm = [](const vec_t& v) {
        double sum = 0;
        for(int i = 0; i < v.dim; ++i) {
            sum += v[i] * v[i];
        }
        return std::sqrt(sum);
    };

    switch(norm) {
        case EUCLID:
            return euclid_norm(v);
        default:
            return -1;
    }
}

struct sqmatrix_t
{
    int     dim;
    double  *data;

    sqmatrix_t();
    sqmatrix_t(const int dim);
    
    sqmatrix_t(const sqmatrix_t& that);
    sqmatrix_t(sqmatrix_t&& that);
    
    ~sqmatrix_t();

    sqmatrix_t& operator=(const sqmatrix_t& that);
    sqmatrix_t& operator=(sqmatrix_t&& that);

    void setdim(const int dim);

    double*         operator[](const int idx);
    const double*   operator[](const int idx) const;

    sqmatrix_t operator*(const float b) const;
    sqmatrix_t operator/(const float b) const;
    sqmatrix_t operator+(const sqmatrix_t& that) const;
    sqmatrix_t operator-(const sqmatrix_t& that) const;

    vec_t operator*(const vec_t& that) const;
};

std::ostream& operator<<(std::ostream& os, const sqmatrix_t& m);
std::istream& operator>>(std::istream& is,       sqmatrix_t& m);

sqmatrix_t::sqmatrix_t()
{
    this->dim = 0;
    this->data = nullptr;
}

sqmatrix_t::sqmatrix_t(const int dim)
{
    this->dim = dim;
    this->data = new double[this->dim * this->dim]{};
}

sqmatrix_t::sqmatrix_t(const sqmatrix_t& that)
{
    new(this) sqmatrix_t(that.dim);
    for(int i = 0; i < this->dim * this->dim; ++i) {
        this->data[i] = that.data[i];
    }
}

sqmatrix_t::sqmatrix_t(sqmatrix_t&& that)
{
    new(this) sqmatrix_t;
    std::swap(this->data, that.data);
    std::swap(this->dim,  that.dim);
}

sqmatrix_t::~sqmatrix_t()
{
    delete[] this->data;
}

sqmatrix_t& sqmatrix_t::operator=(const sqmatrix_t& that)
{
    if(this->dim != that.dim) {
        throw std::runtime_error("Non-compatible assignment");
    }

    for(int i = 0; i < this->dim * this->dim; ++i) {
        this->data[i] = that.data[i];
    }

    return *this;
}

sqmatrix_t& sqmatrix_t::operator=(sqmatrix_t&& that)
{
    if(this->dim != that.dim) {
        throw std::runtime_error("Non-compatible assignment");
    }

    std::swap(this->data, that.data);
    std::swap(this->dim,  that.dim);
    return *this;
}

void sqmatrix_t::setdim(const int dim)
{
    if((this->dim != dim) && (dim > 0)) {
        this->~sqmatrix_t();
        new(this) sqmatrix_t(dim);
    }
}

double* sqmatrix_t::operator[](const int idx)
{
    return this->data + this->dim * idx;
}

const double* sqmatrix_t::operator[](const int idx) const
{
    return this->data + this->dim * idx;
}

sqmatrix_t sqmatrix_t::operator*(const float b) const
{
    sqmatrix_t out(this->dim);

    for(int i = 0; i < out.dim * out.dim; ++i) {
        out.data[i] = this->data[i] * b;
    }
    return out;
}

sqmatrix_t sqmatrix_t::operator/(const float b) const
{
    return (*this) * (1 / b);
}

sqmatrix_t sqmatrix_t::operator+(const sqmatrix_t& that) const
{
    if(this->dim != that.dim) {
        throw std::runtime_error("Non-compatible binary operator");
    }
    
    sqmatrix_t out(this->dim);

    for(int i = 0; i < out.dim * out.dim; ++i) {
        out.data[i] = this->data[i] + that.data[i];
    }
    return out; 
}

sqmatrix_t sqmatrix_t::operator-(const sqmatrix_t& that) const
{
    return (*this) + (that * -1.f);
}

vec_t sqmatrix_t::operator*(const vec_t& that) const
{
    if(this->dim != that.dim) {
        throw std::runtime_error("Non-compatible binary operator");
    }

    vec_t out(this->dim);
    for(int i = 0; i < out.dim; ++i) {
        for(int j = 0; j < this->dim; ++j) {
            out.data[i] += (*this)[i][j] * that[j];
        }
    }
    return out;
}

std::ostream& operator<<(std::ostream& os, const sqmatrix_t& m)
{
    for(int i = 0; i < m.dim; ++i) {
        for(int j = 0; j < m.dim; ++j) {
            os << m[i][j] << " ";
        }
        os << std::endl;
    }
    return os;
}

std::istream& operator>>(std::istream& is, sqmatrix_t& m)
{
    int dim = 0;
    is >> dim;
    m.setdim(dim);

    for(int i = 0; i < m.dim; ++i) {
        for(int j = 0; j < m.dim; ++j) {
            is >> m[i][j];
        }
    }
    return is;
}

// -------------------------------------------------

struct LUsole_t
{
    sqmatrix_t M;
    ~LUsole_t() = default;

    LUsole_t(const sqmatrix_t& A)
    {
        M.setdim(A.dim);
        sqmatrix_t L(A.dim), U(A.dim);

        for(int j = 0; j < A.dim; ++j) {
            U[0][j] = A[0][j];
            L[j][0] = A[j][0] / U[0][0];
        }

        for(int i = 0; i < A.dim; ++i) {
            for(int j = 0; j < A.dim; ++j) {
                double sum_ij = 0;
                double sum_ji = 0;
                for(int k = 0; k < i; ++k) {
                    sum_ij += L[i][k] * U[k][j];
                    sum_ji += L[j][k] * U[k][i];
                }
                U[i][j] = A[i][j] - sum_ij;
                L[j][i] = (A[j][i] - sum_ji) / U[i][i];
            }
        }

        for(int i = 0; i < A.dim; ++i) {
            for(int j = 0; j < A.dim; ++j) {
                M[i][j] = i <= j ? U[i][j] : L[i][j];
            }
        }
    }

    vec_t solve(const vec_t& b)
    {
        vec_t v(M.dim);
        for(int k = 0; k < M.dim; ++k) {
            double sum = 0;
            for(int j = 0; j < k; ++j) {
                sum += M[k][j] * v[j];
            }
            v[k] = b[k] - sum;
        }
        
        vec_t out(M.dim);
        for(int k = M.dim - 1; k >= 0; --k) {
            double sum = 0;
            for(int j = k + 1; j < M.dim; ++j) {
                sum += M[k][j] * out[j];
            }
            out[k] = (v[k] - sum) / M[k][k];
        }
        return out;
    }

};

vec_t exp(const vec_t& v)
{
    vec_t out(v.dim);
    for(int i = 0; i < v.dim; ++i) {
        out[i] = std::exp(v[i]);
    }
    return out;
}

struct F_t
{
    sqmatrix_t A;
    int dim;

    sqmatrix_t J(const vec_t& x) const
    {
        sqmatrix_t out(A.dim);
        for(int i = 0; i < A.dim; ++i) {
            for(int j = 0; j < A.dim; ++j) {
                out[i][j] = A[i][j] + ((i == j) ? std::exp(-x[i]) : 0);
            }
        }
        return out;
    }

    F_t(const sqmatrix_t& m): A(m), dim(A.dim) {}

    vec_t operator()(const vec_t& u) const
    {
        return A * u - exp(-u);
    }
};

template<typename F>
vec_t solve_newton( const F& f,
                    const vec_t& initial_guess = vec_t(),
                    const double res_stop = 1e-6,
                    std::ostream& os = std::cout,
                    bool output = true)
{
    vec_t out = initial_guess;
    out.setdim(f.dim);

    double res = norm(f(out));
    while(res > res_stop) {
        if(output) {
            os << "res = " << res << " u = " << out << std::endl;
        }

        sqmatrix_t J = f.J(out);

        LUsole_t sole(J);

        out = sole.solve(J * out - f(out));

        res = norm(f(out));
    }
    return out;
}

int main(int argc, char **argv)
{
    if(argc != 2) {
        throw std::runtime_error("Wrong amount of arguments");
    }
    std::ifstream is(argv[1]);
    if(!is.is_open()) {
        throw std::runtime_error("Can't open file [" + std::string(argv[1]) + "]");
    }

    sqmatrix_t A;
    is >> A;
    is.close();
    
    F_t f(A);

    std::cout << std::endl << solve_newton(f) << std::endl;

    return 0;
}